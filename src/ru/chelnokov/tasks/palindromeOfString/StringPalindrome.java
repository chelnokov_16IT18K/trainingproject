package ru.chelnokov.tasks.palindromeOfString;

import java.util.Scanner;

public class StringPalindrome {
    private static Scanner reader = new Scanner(System.in);

    public static void main(String args[]) {
        String inputString = correctEntering();
        char[] charArray = inputString.toCharArray();
        if (isPalindrome(charArray)) {
            System.out.println("Палиндром");
        } else {
            System.out.println("Не палиндром");
        }
    }

    /**
     * Проверяет является ли строка палиндромом
     *
     * @param stringToChar массив символов введенной строки
     * @return значение типа boolean (1 - если строка палиндром, 0 в любом другом случае)
     */
    private static boolean isPalindrome(char[] stringToChar) {
        for (int i = 0; i < stringToChar.length / 2; i++) {
            if (stringToChar[i] != stringToChar[stringToChar.length - i - 1]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Считывает строку, введеную пользователем и возвращает её
     *
     * @return строка, введенная пользователем
     */
    private static String correctEntering() {
        String inputString;
        System.out.println("Введите проверяемую строку.");
        do {
            inputString = reader.nextLine();
            inputString = inputString.toLowerCase().replaceAll("[^a-zа-я0-9]", "");
            if (inputString.isEmpty()) {
                System.out.println("Введена пустая или недопустимая строка! Попробуйте снова.");
            }
        } while (inputString.isEmpty());
        return inputString;
    }
}