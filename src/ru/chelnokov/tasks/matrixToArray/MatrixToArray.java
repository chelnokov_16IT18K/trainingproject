package ru.chelnokov.tasks.matrixToArray;

import java.util.Arrays;
import java.util.Scanner;

public class MatrixToArray {
    private static Scanner reader = new Scanner(System.in);

    public static void main(String args[]) {
        System.out.println("Ведите количство строк");
        int line = reader.nextInt();
        System.out.println("Ведите количство столбцов");
        int column = reader.nextInt();
        int[][] matx = new int[line][column];
        initMatx(matx);
        printMatx(matx);
        int[] arr = new int[matx.length * matx[0].length];
        matxToArr(matx, arr);
        System.out.println("Одномерный массив");
        System.out.println(Arrays.toString(arr));
    }

    /**
     * Заполняет матрицу случайными числами
     *
     * @param matx матрица
     */
    private static void initMatx(int[][] matx) {
        int i, j;
        for (i = 0; i < matx.length; i++) {
            for (j = 0; j < matx[i].length; j++) {
                matx[i][j] = (int) (Math.random() * 10);
            }
        }
    }

    /**
     * Выводит матрицу
     *
     * @param matx матрица
     */
    private static void printMatx(int[][] matx) {
        for (int i = 0; i < matx.length; ++i, System.out.println()) {
            for (int j = 0; j < matx[i].length; j++) {
                System.out.print(matx[i][j] + " ");
            }
        }
    }

    /**
     * Переносит построчно элементы матрицы в массив
     *
     * @param matx матрица
     * @param arr  массив
     */
    private static void matxToArr(int[][] matx, int[] arr) {
        int g = 0;
        for (int[] aMatx : matx) {
            for (int anAMatx : aMatx) {
                arr[g] = anAMatx;
                g++;
            }
        }
    }
}