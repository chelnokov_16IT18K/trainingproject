package ru.chelnokov.tasks.readToThePoint;

import java.util.Scanner;

public class ReadToThePoint {
    private static Scanner reader = new Scanner(System.in);

    public static void main(String args[]) {
        String newString;
        int space = 0;
        String string;
        System.out.println("Введите строку");
        string = reader.nextLine();
        int indexOfDot = string.indexOf(".");
        if(indexOfDot == -1){
            System.out.println("Точки нет. Провести меня пытался? М?");
            newString = string;
        }else{
            newString = string.substring(0, indexOfDot);
        }
        space = getSpace(newString, space);
        System.out.println(newString);
        System.out.println("Количество пробелов в строке: " + space);
    }

    /**
     * Метод,вычисляющий количество пробелов в строке
     * @param newString данная строка
     * @param space количество пробелов
     * @return количество пробелов
     */
    private static int getSpace(String newString, int space) {
        for (int i = 0; i < newString.length(); i++) {
            char a = newString.charAt(i);
            if (a==' ') {
                space++;
            }
        }
        return space;
    }
}
