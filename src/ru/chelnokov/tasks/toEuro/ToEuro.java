package ru.chelnokov.tasks.toEuro;


import java.util.Scanner;

public class ToEuro {
    public static void main(String[]args){
        Scanner reader = new Scanner(System.in);
        double rub;
        double euro;
        double course;
        System.out.println("Введите курс евро");
        course = reader.nextDouble();
        while(course <= 0){
            System.out.println("Курс евро введён некорректно.Попробуйте снова.");
            course = reader.nextDouble();
        }
        System.out.println("Введите количество рублей, которое хотите перевести");
        rub = reader.nextDouble();
        while(rub <= 0){
            System.out.println("Количество рублей введено некорректно.Попробуйте снова");
            rub = reader.nextDouble();
        }
        euro = rub / course;
        System.out.print(rub + " рублей = ");
        System.out.printf("%.2f" ,euro);
        System.out.println(" евро.");
    }
}
