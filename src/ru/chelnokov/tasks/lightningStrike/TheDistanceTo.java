package ru.chelnokov.tasks.lightningStrike;

public class TheDistanceTo {
    public static void main(String[] args) {
        double seconds = 6.8;
        double speed = 1234.8;
        double hours = seconds / 3600;
        double distance = speed * hours;
        System.out.println("Вы находитесь на расстоянии " + distance + " км. от места удара молнии");
    }
}
