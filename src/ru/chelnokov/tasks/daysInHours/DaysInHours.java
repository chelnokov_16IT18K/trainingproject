package ru.chelnokov.tasks.daysInHours;

import java.util.Scanner;

public class DaysInHours {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Сколько дней прошло, мой друг?");
        int days = reader.nextInt();
        System.out.println("А я всё не по дням, а по часам");
        System.out.println("В часах: " + days * 24);
        System.out.println("В минутах: " + days * 24 * 60);
        System.out.println("В секундах: " + days * 24 * 3600);
    }
}
