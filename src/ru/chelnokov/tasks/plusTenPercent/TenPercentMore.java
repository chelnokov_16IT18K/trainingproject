package ru.chelnokov.tasks.plusTenPercent;

import java.util.Arrays;
import java.util.Scanner;

public class TenPercentMore {
    public static void main(String[] args) {
        double[] array = {10, 5, 7, 4, 6, 43, 11, 1488, 228, 69};
        Scanner reader = new Scanner(System.in);
        System.out.println("Исходная матрица: ");
        System.out.println(Arrays.toString(array));
        System.out.println("Введите номер элемента(их всего десять)");
        int number = reader.nextInt();
        while (number < 1 || number > array.length){
            System.out.println("Неправильно набран номер. Абонент не абонент. Попробуйте снова");
            number = reader.nextInt();
        }
        plusTenPercent(array, number);
    }
    private static void plusTenPercent(double array[], int number){
        number--;
        array[number] = array[number] + (array[number]/10);
        System.out.println("Теперь ваше чудо выглядит так:");
        System.out.println(Arrays.toString(array));
        System.out.println("Идеально.");
    }
}

