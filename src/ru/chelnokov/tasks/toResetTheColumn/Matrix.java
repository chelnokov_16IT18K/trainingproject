package ru.chelnokov.tasks.toResetTheColumn;

import java.util.Scanner;

public class Matrix {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        int[][] matrixA = new int[3][5];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 5; j++) {
                matrixA[i][j] = 1 + (int) (Math.random() * 9);
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(matrixA[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println("Введите номер столбца, который хотите обнулить:");
        int number;
        number = reader.nextInt();
        while (number < 1 || number > 5){
            System.out.print("Давайте ещё раз, голубчик: ");
            number = reader.nextInt();
        }

        System.out.println("Теперь ваша матрица выглядит так:");
        reseter(matrixA, number);
    }

    private static void reseter(int[][] matrixA, int number) {//"обнулитель"
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 5; j++) {
                matrixA[i][number - 1] = 0;
                System.out.print(matrixA[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
