package ru.chelnokov.tasks.palindromeOfNumbers;

import java.util.Scanner;

public class NumberPalindrome {
    private static Scanner reader = new Scanner(System.in);

    public static void main(String args[]) {
        System.out.println("Введите число.");
        double num = reader.nextDouble();
        String inputString = String.valueOf(num).replace(".", "");
        char[] charArray = inputString.toCharArray();//переводим введённую строку в массив типа char
        if (isPalindrome(charArray)) {
            System.out.println("Палиндром");
        } else {
            System.out.println("Не палиндром");
        }
    }

    /**
     * Проверяет является ли число палиндромом
     *
     * @param chars массив символов введенной строки
     * @return значение типа boolean (1 - если строка палиндром, 0 в любом другом случае)
     */
    private static boolean isPalindrome(char[] chars) {
        for (int i = 0; i < chars.length / 2; i++) {
            if (chars[i] != chars[chars.length - i - 1]) {
                return false;
            }
        }
        return true;
    }
}
