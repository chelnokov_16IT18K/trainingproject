package ru.chelnokov.tasks.multiplicationTable;

import java.util.Scanner;

public class MultiplicationTable {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Введите число");
        int number = reader.nextInt();
        for (int i = 2; i <= 10;i++){
            System.out.println(number + " x " + i + " = " + (number * i));
        }
    }
}

